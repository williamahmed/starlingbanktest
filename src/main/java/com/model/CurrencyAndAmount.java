package com.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CurrencyAndAmount {

    private String currency;

    private int minorUnits;


    public CurrencyAndAmount(String currency, int minorUnits) {
        this.currency = currency;
        this.minorUnits = minorUnits;
    }

    public CurrencyAndAmount(String currency, BigDecimal amount) {
        this.currency = currency;
        this.minorUnits = amount.multiply(new BigDecimal(100).setScale(0, RoundingMode.UP)).intValue();
        System.out.println(this.minorUnits);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getMinorUnits() {
        return minorUnits;
    }

    public void setMinorUnits(int minorUnits) {
        this.minorUnits = minorUnits;
    }

    @Override
    public String toString() {
        return "CurrencyAndAmount{" +
                "currency='" + currency + '\'' +
                ", minorUnits=" + minorUnits +
                '}';
    }
}
