package com.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transactions {

    @JsonProperty("transactions")
    Collection<Transaction> transactions;

    public Collection<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Collection<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "transactions=" + transactions +
                '}';
    }

    public static Transactions mashallJsonObjectToTransactions(JSONObject jsonObject) {
        ObjectMapper mapper = new ObjectMapper();
        Transactions transactions = null;

        try {
            transactions = mapper.readValue(jsonObject.get("_embedded").toString(), Transactions.class);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return transactions;
    }
}
