package com.model;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class ObjectMarshaller {

    public static String unmarshallObject(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        String response = "";
        try {

            response = objectMapper.writeValueAsString(object);
            System.out.println(response);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }
}
