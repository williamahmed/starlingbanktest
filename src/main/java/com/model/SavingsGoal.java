package com.model;


public class SavingsGoal {


    private String name;


    private String currency;


    private CurrencyAndAmount target;


    public SavingsGoal(String name, String currency, CurrencyAndAmount target) {
        this.name = name;
        this.currency = currency;
        this.target = target;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public CurrencyAndAmount getTarget() {
        return target;
    }

    public void setTarget(CurrencyAndAmount target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "SavingsGoal{" +
                "name='" + name + '\'' +
                ", currency='" + currency + '\'' +
                ", target=" + target +
                '}';
    }
}
