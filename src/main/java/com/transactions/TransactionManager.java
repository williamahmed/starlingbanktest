package com.transactions;

import com.model.Transactions;
import com.net.http.HttpClient;
import com.net.http.HttpRequestMethod;
import com.net.http.HttpResponse;

import java.net.URL;

public class TransactionManager {

    /**
     * Makes a Http call to a URL representing the GET Transactions REST API
     * @param url
     * @return the Transactions associated with the given account
     */
    public Transactions retrieveTransactionsFromUrl(URL url) {

        Transactions transactions = null;

        HttpClient httpClient = new HttpClient();
        HttpResponse httpResponse = httpClient.performHttpRequestWithoutPayload(HttpRequestMethod.GET, url);
        System.out.println(httpResponse.getResponseAsJsonObject().toString());

        transactions = Transactions.mashallJsonObjectToTransactions(httpResponse.getResponseAsJsonObject());


        return transactions;

    }

}
