package com;

import com.model.CurrencyAndAmount;
import com.model.CurrencyAndAmountWrapper;
import com.model.Transactions;
import com.net.http.HttpResponse;
import com.savings.SavingsManager;
import com.transactions.TransactionManager;
import org.json.JSONException;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        TransactionManager transactionManager = new TransactionManager();
        SavingsManager savingsManager = new SavingsManager();

        String accountUid = "9ca0e14f-ffde-4ce0-b599-9a2f535c5c1b";

        try {
            // Handle Transactions
            URL transactionAPIUrl = new URL("https://api-sandbox.starlingbank.com/api/v1/transactions");
            Transactions transactions = transactionManager.retrieveTransactionsFromUrl(transactionAPIUrl);

            BigDecimal totalSavingsAmount = savingsManager.calculateSavings(transactions);

            CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount("GBP", totalSavingsAmount);
            CurrencyAndAmountWrapper amount = new CurrencyAndAmountWrapper();
            amount.amount = currencyAndAmount;

            //Create savings pot
            URL savingsPotCreateUrl =
                    new URL("https://api-sandbox.starlingbank.com/api/v2/account/9ca0e14f-ffde-4ce0-b599-9a2f535c5c1b/savings-goals");
            HttpResponse savingsPotCreateResponse = savingsManager.createSavingsPotAtUrl(savingsPotCreateUrl);

            String savingsPotUid = savingsPotCreateResponse.getResponseAsJsonObject().get("savingsGoalUid").toString();

            URL addMoneyToSavingsPotUrl =
                    new URL(String.format("https://api-sandbox.starlingbank.com/api/v2/account/%s/savings-goals/%s/add-money/%s",
                    accountUid, savingsPotUid, UUID.randomUUID().toString()));
            System.out.println(addMoneyToSavingsPotUrl.toString());

            HttpResponse adddMoneyToSavingsPotResponse = savingsManager.addAmountToSavingsPotAtUrl(addMoneyToSavingsPotUrl, amount);




        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
