package com.savings;

import com.model.*;
import com.net.http.HttpClient;
import com.net.http.HttpRequestMethod;
import com.net.http.HttpResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;

public class SavingsManager {

    /**
     * Iterates over a Transactions object containing a collection of individual Transaction
     * objects. For each transaction, the value is rounded to the nearest major unit
     * of currency and the amount is returned.
     * @param transactions
     * @return totalSavingsAmount
     */
    public BigDecimal calculateSavings(final Transactions transactions) {

        BigDecimal totalSavingsAmount = new BigDecimal(0);


        for(Transaction t: transactions.getTransactions()) {

            System.out.println(String.format("CurrencyAndAmountWrapper: %s, Balance: %s", t.getAmount(), t.getBalance()));

            //Only look at debit transactions
            if(t.getAmount() < 0) {

                BigDecimal unsignedAmount = new BigDecimal(t.getAmount()).abs();
                BigDecimal roundedAmount = unsignedAmount.setScale(0, RoundingMode.UP);

                //Ensure that the balance is sufficient to handle the round up of a transaction
                //Could provide opinionated code to round transaction amount up to available balance if
                // sufficient balance is not present.
                if(roundedAmount.compareTo(new BigDecimal(t.getBalance())) != 1) {
                    BigDecimal savingsAmount = roundedAmount.subtract(unsignedAmount);
                    totalSavingsAmount = totalSavingsAmount.add(savingsAmount);
                }

            }

        }

        return totalSavingsAmount;

    }

    public HttpResponse createSavingsPotAtUrl(final URL url) {

        SavingsGoal savingsGoal = new SavingsGoal(
                "New Car Savings Goal",
                "GBP",
                new CurrencyAndAmount("GBP", 120483)
        );



        String payload = ObjectMarshaller.unmarshallObject(savingsGoal);
        System.out.println(payload);

        HttpClient httpClient = new HttpClient();
        HttpResponse response = httpClient.performHttpRequestWithPayload(HttpRequestMethod.PUT, url, payload);
        System.out.println(response.getResponseAsJsonObject().toString());
        return response;
    }

    public HttpResponse addAmountToSavingsPotAtUrl(final URL url, final CurrencyAndAmountWrapper currencyAndAmount) {
        String payload = ObjectMarshaller.unmarshallObject(currencyAndAmount);

        HttpClient httpClient = new HttpClient();
        HttpResponse response = httpClient.performHttpRequestWithPayload(HttpRequestMethod.PUT, url, payload);
        return response;
    }

}
