package com.validation;

import com.exception.HttpValidationException;

import java.net.MalformedURLException;
import java.net.URL;

public class HttpValidation {


    public static void validateUrlNotNull(final URL url) throws HttpValidationException {
        if(url == null) {
            throw new HttpValidationException("Null URL provided");
        }
    }

    public static void validateUrlProtocolNotEmpty(final URL url) throws MalformedURLException {
        if(url.getProtocol() == null) {
            throw new MalformedURLException("Null protocol found in URL");
        }
    }

}
