package com.validation;

import com.exception.StringValidationException;

public class StringValidation {

    public static void validateNotNullOrEmpty(final String val) throws StringValidationException {
        if(val == null || val.isEmpty() || val == "") {
            throw new StringValidationException("String null or empty");
        }
    }
}
