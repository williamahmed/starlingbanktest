package com.net.http;

public enum HttpRequestMethod {

    /*
    Included all Http Request Methods for completeness.

    Will only need GET and PUT for this particular code.
     */

    GET,

    POST,

    PUT,

    DELETE,

    HEAD,

    CONNECT,

    OPTIONS,

    TRACE

}
