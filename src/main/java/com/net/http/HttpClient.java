package com.net.http;

import java.net.URL;

public class HttpClient {

    /**
     * Performs a HTTP API request that requires a payload.
     * @param method
     * @param url
     * @param payload
     * @return HttpResponse - the response from the rest service
     */
    public HttpResponse performHttpRequestWithPayload(final HttpRequestMethod method,
                                           final URL url,
                                           final String payload) {

        HttpRequestHeader httpRequestHeader = buildRequestHeaders(method);

        HttpRequest httpRequest = new HttpRequest(method, httpRequestHeader, url, payload);
        HttpResponse response =  httpRequest.performHttpRequest();

        return response;
    }

    /**
     * Performs an HTTP API request that does not require a payload
     * @param method
     * @param url
     * @return HttpResponse - the Response from the REST service
     */
    public HttpResponse performHttpRequestWithoutPayload(final HttpRequestMethod method,
                                           final URL url) {

        HttpRequestHeader httpRequestHeader = buildRequestHeaders(method);

        HttpRequest httpRequest = new HttpRequest(method, httpRequestHeader, url);
        HttpResponse response =  httpRequest.performHttpRequest();

        return response;
    }

    /**
     * Builds the request headers for an API call.
     * @param requestMethod
     * @return
     */
    private HttpRequestHeader buildRequestHeaders(HttpRequestMethod requestMethod) {
        HttpRequestHeader httpRequestHeader = new HttpRequestHeader();
        httpRequestHeader.addRequestHeader("Authorization", "Bearer uetvd8ALiPJV7JUJvFFEg3PoWwYwxBf0Ng4SoUAbdCPnMvd0LyYV60tshYpBJ1XZ");
        httpRequestHeader.addRequestHeader("User-Agent", "William Ahmed");

        switch(requestMethod) {
            case GET:
                httpRequestHeader.addRequestHeader("Accept", "application/json");
            break;
            case PUT:
                httpRequestHeader.addRequestHeader("Content-Type", "application/json");
            break;
        }

        return httpRequestHeader;
    }

}
