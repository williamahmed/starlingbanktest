package com.net.http;

import com.exception.HttpValidationException;
import com.exception.StringValidationException;
import com.validation.HttpValidation;
import com.validation.StringValidation;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class HttpRequest {

    private HttpRequestMethod httpRequestMethod;
    private URL url;
    private HttpRequestHeader headers;
    private String payload;


    /**
     * Constructor for http requests that do not include a payload
     * @param httpRequestMethod
     * @param url
     */
    public HttpRequest(HttpRequestMethod httpRequestMethod, HttpRequestHeader headers, URL url) {
        this.httpRequestMethod = httpRequestMethod;
        this.headers = headers;
        this.url = url;
    }

    /**
     * Constructor for Http Calls that include a payload
     * @param httpRequestMethod
     * @param url
     * @param payload
     */
    public HttpRequest(HttpRequestMethod httpRequestMethod, HttpRequestHeader headers, URL url, String payload) {
        this.httpRequestMethod = httpRequestMethod;
        this.headers = headers;
        this.url = url;
        this.payload = payload;
    }

    /**
     * Performs a HTTP call out to the specified URL with the given
     * HttpRequestMethod (@see com.net.http.HttpRequestMethod)
     *
     * @return The response from the HttpRequest
     */
    public HttpResponse performHttpRequest() {

        HttpResponse httpResponse;

        try {

            //Perform defensive checks on URL to ensure no obvious missing data
            HttpValidation.validateUrlNotNull(this.url);
            HttpValidation.validateUrlProtocolNotEmpty(this.url);

            // Create the URL connection through a helper method that adds the HttpRequestHeaders on the
            //URL Connection object
            URLConnection connection = buildUrlConnectionWithHttpRequestHeaders(this.url.openConnection());

            //Cast the generic URL Connection to a HttpUrlConnection
            HttpURLConnection httpURLConnection = (HttpURLConnection)connection;

            //Set the request method
            httpURLConnection.setRequestMethod(this.httpRequestMethod.name());

            if(httpURLConnection.getRequestMethod() == HttpRequestMethod.POST.name() ||
                    httpURLConnection.getRequestMethod() == HttpRequestMethod.PUT.name()) {

                //Validate that the payload is not null or empty
                StringValidation.validateNotNullOrEmpty(this.payload);

                //Write the payload to the output stream
                httpURLConnection.setDoOutput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                outputStream.write(this.payload.getBytes());

                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            int responseCode = httpURLConnection.getResponseCode();


            //Check if the Http Request was successful before reading response
            if(responseCode != 200) {
                System.out.println(httpURLConnection.getResponseCode());
                System.out.println(httpURLConnection.getResponseMessage());

            } else {

                httpResponse = readHttpResponseFromHttpUrlConnectionObject(httpURLConnection);
                return httpResponse;
            }


        } catch (HttpValidationException | IOException | StringValidationException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Reads the response from a Http request.
     * @param httpURLConnection
     * @return
     */
    private HttpResponse readHttpResponseFromHttpUrlConnectionObject(final HttpURLConnection httpURLConnection) {

        InputStream inputStream;
        InputStreamReader inputStreamReader;
        BufferedReader bufferedReader;

        String result = "";

        try {
            //Read from the input stream provided by the HttpURL Connection
            inputStream = httpURLConnection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream);

            bufferedReader = new BufferedReader(inputStreamReader);

            String currentLine;
            StringBuffer response = new StringBuffer();

            while((currentLine = bufferedReader.readLine()) != null) {
                response.append(currentLine);
            }

            result = response.toString();

            inputStream.close();
            inputStreamReader.close();
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }

        return new HttpResponse(result);
    }


    private URLConnection buildUrlConnectionWithHttpRequestHeaders(final URLConnection connection) {

        this.headers.getHeaders().entrySet().forEach(
                (entry) -> connection.addRequestProperty(entry.getKey(), entry.getValue())
        );

        return connection;
    }


}
