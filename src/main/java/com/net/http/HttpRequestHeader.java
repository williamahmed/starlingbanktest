package com.net.http;

import java.util.Map;
import java.util.TreeMap;

public class HttpRequestHeader {

    private Map<String, String> headers;

    public HttpRequestHeader() {
        this.headers = new TreeMap<>();
    }

    public void addRequestHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    @Override
    public String toString() {
        return "HttpRequestHeader{" +
                "headers=" + headers +
                '}';
    }
}
