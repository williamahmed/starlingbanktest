package com.net.http;

import org.json.JSONException;
import org.json.JSONObject;

public class HttpResponse {

    private JSONObject responseObject;

    public HttpResponse(String response) {
        try {
            this.responseObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getResponseAsJsonObject() {
        return this.responseObject;
    }
}
