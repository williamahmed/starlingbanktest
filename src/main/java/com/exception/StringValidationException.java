package com.exception;

public class StringValidationException extends Exception {

    public StringValidationException(String message) {
        super(message);
    }
}
