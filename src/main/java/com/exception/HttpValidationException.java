package com.exception;

/**
 * Exception for Http Validation. Provides more granular output in the event of error.
 */
public class HttpValidationException extends Exception {

    public HttpValidationException(String message) {
        super(message);
    }
}
